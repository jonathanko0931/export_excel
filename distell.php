<?php
require "vendor/autoload.php";
require "util/Export.php";

use util\Config;
use util\ExportExcel;

date_default_timezone_set("Asia/Taipei");
$config = new Config();
$export = new ExportExcel();
$DBName = "distell";
$DB = new mysqli(
    $config->DBHost,
    $config->DBUser,
    $config->DBPassword,
    $DBName
);
$now = str_replace("-", "", date("Y-m-d"));
$filename = "樂利豐_仕高利達第二檔數據報表_".$now;
try {
    $sql = "SELECT * FROM (SELECT COUNT(a.id) AS count_id
FROM users a
WHERE a.created_at >= '2021-07-29 00:00:00') aa
inner JOIN
(SELECT a.status, a.certification_status, a.created_at as invoice_date, b.phone_id, b.name, b.created_at , b.age, a.number,a.by_date,
(case a.from
when 'invoice' then '電子 - 照片'
when  'write' then '電子 - 手動'
when 'invoiceReceipt' then '雲端 - 手機條碼'
when 'invReceiptWrite' then '雲端 - 其他載具'
when 'tradition' then '傳統發票'
END) AS 'from',
 a.item_all, a.unitPrices, a.ec_seller_name, a.seller_address
FROM invoice a
RIGHT JOIN users b
ON a.phone_id = b.phone_id
WHERE a.by_date >= '2021-10-20 00:00:00'
ORDER BY invoice_date) bb";
    $db = $DB->query($sql);
    $res = array();
    $i = 1;
    if ($db->num_rows > 0) {
        while ($row = $db->fetch_assoc()) {
            $res[0] = array(
                "會員人數",
                "發票辨識",
                "發票認證狀態",
                "發票登錄時間",
                "會員手機",
                "會員姓名",
                "會員註冊日期",
                "會員年紀",
                "發票號碼",
                "發票日期",
                "發票類型",
                "發票品項",
                "發票金額",
                "發票開立公司",
                "賣方地址"
            );
            $res[1] = array(
                $row["count_id"],
                $row["status"],
                $row["certification_status"],
                $row["invoice_date"],
                "\t" . $row["phone_id"],
                $row["name"],
                $row["created_at"],
                $row["age"],
                $row["number"],
                $row["by_date"],
                $row["from"],
                $row["item_all"],
                $row["unitPrices"],
                "\t" . $row["ec_seller_name"],
                $row["seller_address"]
            );
            $res[$i + 1] = array(
                $row[null],
                $row["status"],
                $row["certification_status"],
                $row["invoice_date"],
                "\t" . $row["phone_id"],
                $row["name"],
                $row["created_at"],
                $row["age"],
                $row["number"],
                $row["by_date"],
                $row["from"],
                $row["item_all"],
                $row["unitPrices"],
                "\t" . $row["ec_seller_name"],
                $row["seller_address"]
            );
            $i++;
        }
    } else {
        echo "0 results";
    }
    $export->exportExcel($res, $filename, $DBName);
} catch (Exception $e) {
    print("something wrong" . $e);
}
$DB->close();
