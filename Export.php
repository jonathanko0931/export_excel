<?php
namespace util;

use Exception;
use JetBrains\PhpStorm\Pure;
//use util\Log;
//use util\Config;

error_reporting(E_ERROR | E_PARSE);
class ExportExcel
{
    private \util\Log $log;

    #[Pure] public function __construct()
    {
        $this->log = new Log();
    }

    public function exportExcel($res, $filename, $DirName)
    {
        try {
            $file = fopen($filename.'.csv', "w+", TRUE);
            fprintf($file, chr(0xEF) . chr(0xBB) . chr(0xBF));
            foreach ($res as $field) {
                fputcsv($file, $field, ",", '"');
            }
            print("File Complete Successfully!!!\n");
            $this->log->file_put_logs(array('status' => "success", 'rs' => $res), './log' . '/' . $DirName, $DirName . '_send_' . str_replace("-", "_", date("Y-m-d")) . '_log');
            print("Log Send Successfully!!!\n");
            fclose($file);
        } catch (Exception $e) {
            $this->log->file_put_logs(array('status' => "failure", 'rs' => $e), './log' . '/' . $DirName, $DirName . '_error_' . str_replace("-", "_", date("Y-m-d")) . '_log');
            print("Error Log Send Successfully!!!\n");
        }
    }
}
