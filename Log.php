<?php
namespace util;
class Log{
    public function file_put_logs($msg,$log_path,$file_name){
        if(!is_dir($log_path)) mkdir($log_path,0777,true);
        $log_path .= '/'.$file_name;
        file_put_contents($log_path, date("Y-m-d H:i:s ") . "\n" . print_r($msg, true) . "\n", FILE_APPEND);
    }
}

